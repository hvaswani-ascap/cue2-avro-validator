
### JSON - Avro Validator ###

* This app validates a JSON input (as an input File or String) against an Avro Schema 

### Set up and running the app ###

* Look under src/main/java for the main validator code
* Look under src/main/test for unit tests
* Unit tests test variety of input JSONs against a sample Avro schema
* Schema includes string, int and nested object data types

### Who do I talk to? ###

* @HarishVaswani