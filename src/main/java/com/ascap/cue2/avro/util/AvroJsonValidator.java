package com.ascap.cue2.avro.util;

import org.apache.avro.AvroTypeException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;

import java.io.*;

public class AvroJsonValidator {

    public static boolean validate(File schemaFile, File jsonFile) throws Exception {
        InputStream input = new FileInputStream(jsonFile);
        return parse(schemaFile, input);

    }

    public static boolean validate(File schemaFile, String json) throws Exception {
        InputStream input = new ByteArrayInputStream(json.getBytes());
        return parse(schemaFile, input);
    }

    private static boolean parse(File schemaFile, InputStream inputStream) throws Exception {

        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(schemaFile);
        DataInputStream din = new DataInputStream(inputStream);

        try {
            DatumReader reader = new GenericDatumReader(schema);
            Decoder decoder = DecoderFactory.get().jsonDecoder(schema, din);
            reader.read(null, decoder);
            return true;
        } catch (AvroTypeException e) {

            e.printStackTrace();
            return false;
        }
    }
}
