package com.ascap.cue2.avro.util;

import org.junit.*;

import java.io.File;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class AvroJsonValidatorTest {

    private static final String SAMPLE_AVRO_SCHEMA = "sample-schema.avsc";
    private static final String SAMPLE_CUESHEET_SCHEMA = "sample-cuesheet-schema.avsc";

    @Test
    public void testGoodJson() throws Exception {

        File schemaFile = new File(getClass().getClassLoader().getResource(SAMPLE_AVRO_SCHEMA).getFile());
        File jsonFile = new File(getClass().getClassLoader().getResource("sample-good.json").getFile());
        assertTrue(AvroJsonValidator.validate(schemaFile, jsonFile));
    }

    @Test
    public void testMissingFieldBadJson() throws Exception {
        File schemaFile = new File(getClass().getClassLoader().getResource(SAMPLE_AVRO_SCHEMA).getFile());
        File jsonFile = new File(getClass().getClassLoader().getResource("sample-missing-fields-bad.json").getFile());
        assertFalse(AvroJsonValidator.validate(schemaFile, jsonFile));
    }

    @Test
    public void testNullFieldGoodJson() throws Exception {
        File schemaFile = new File(getClass().getClassLoader().getResource(SAMPLE_AVRO_SCHEMA).getFile());
        File jsonFile = new File(getClass().getClassLoader().getResource("sample-null-field-good.json").getFile());
        assertTrue(AvroJsonValidator.validate(schemaFile, jsonFile));
    }

    @Test
    public void testSampleCuesheetJson() throws Exception {
        File schemaFile = new File(getClass().getClassLoader().getResource(SAMPLE_CUESHEET_SCHEMA).getFile());
        File jsonFile = new File(getClass().getClassLoader().getResource("sample-cuesheet.json").getFile());
        assertTrue(AvroJsonValidator.validate(schemaFile, jsonFile));
    }
}
